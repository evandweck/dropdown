import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import _ from 'lodash'

// antd thingz
import 'antd/dist/antd.css' // or 'antd/dist/antd.less'
import { Select } from 'antd'
const { Option } = Select

const StyledOptionContainer = styled.div`
  display: flex;
  flex-direction: row;
`

const OptionContainer = styled.div`
  padding: 3px 0;
`

const OptionDescription = styled.div`
  color: #c2c5cc;
  font-size: 14px;
`

const OptionValue = styled.div`
  color: #e66465;
`

const OptionPlaceHolderValue = styled.div`
`

const OptionValueIcon = styled.div`
`

const StyledSelect = styled(Select)`
  .ant-select-selection-selected-value ${OptionContainer}
  .ant-select-selection_choice_content ${OptionContainer} {
    padding: 0;
  }

  .ant-select-selection {
    border-top: none;
    border-left: none;
    border-right: none;
    border-radius: 0px;
    padding-top: 0;
    padding-left: 0;
    padding-right: 0;
    margin-left: 0;
    margin-right: 0;
    height: 36px;
    line-height: 34px;
  }
`

export default class Dropdown extends Component {
  getSize() {
    const { size } = this.props
    if (size === 'small') return '250px'
    else if (size === 'medium') return '400px'
    else if (size === 'large') return '550px'
  }

  renderOption ({key, value, description, icon, iconColor, title, isPlaceholder}) {
    const children = (
      <OptionContainer title={title}>
        {isPlaceholder
          ? <OptionPlaceHolderValue>{value}</OptionPlaceHolderValue>
          : (icon
            ? <StyledOptionContainer>
              <OptionValueIcon>{icon}</OptionValueIcon>
              <OptionValue>{value}</OptionValue>
            </StyledOptionContainer>
            : <OptionValue>{value}</OptionValue>
          )
        }
        {description ? <OptionDescription title={description}>{description}</OptionDescription> : null}
      </OptionContainer>
    )

    return (
      <Option
        value={key}
        key={`dropdown-option-${key}-${value}`}
      >
        {children}
      </Option>
    )
  }

  render() {
    const { options, placeholder } = this.props
    const mappedOptions = _.map(options, (option) => this.renderOption(option))
    const width = this.getSize()

    return (
      <StyledSelect
        showSearch
        style={{ width }}
        placeholder={placeholder}
        onChange={this.props.onChange}
        onFocus={this.props.onFocus}
        onBlur={this.props.onBlur}
        filterOption={(input, option) =>
          option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
        }
      >
        {mappedOptions}
      </StyledSelect>
    )
  }
}

Dropdown.defaultProps = {
  options: [],
  placeholder: 'Select an Option',
  size: 'medium',
  onChange: () => null,
  onFocus: () => null,
  onBlur: () => null
}

Dropdown.propTypes = {
  options: PropTypes.array.isRequired,
  placeholder: PropTypes.string,
  size: PropTypes.string,
  onChange: PropTypes.func,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func
}
