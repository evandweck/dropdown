# dropdown-sample

> dropdown component library

[![NPM](https://img.shields.io/npm/v/dropdown.svg)](https://www.npmjs.com/package/dropdown) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install the dropdown component

```bash
npm i --save dropdown-sample
```

## Install dependencies (antd & styled components 💅)

```bash
npm i --save antd 
npm i --save styled-components 
```



## Usage

```jsx
import React, { Component } from 'react'

import Dropdown from 'dropdown-sample'

class Example extends Component {
  render () {
    return (
      <Dropdown 
        options={[
          { key: 'one', value: 'A simple option' },
          { key: 'two', value: 'A second option' },
          { key: 'three', value: 'Third and final!' },
          { key: 'four', value: 'mangos' },
        ]}
        placeholder="Select an Option"
        size="small"
        onChange={() => {}}
        onFocus={() => {}}
        onBlur={() => {}}
      />
    )
  }
}
```

usage with icon emojis.

```jsx
<Dropdown 
  options={[
    {key: 'one', value: 'strawberry', icon: '🍓'},
    {key: 'two', value: 'watermelon', icon: '🍉' },
    {key: 'three', value: 'mango', icon: '🥭' },
    {key: 'four', value: 'avocado', icon: '🥑' },
    {key: 'five', value: 'pear', icon: '🍐' },
  ]}
  placeholder="Select an option (w/ a cute fruit icon!)"
  size="medium"
  onChange={() => console.log('On change FIRED 🚀')}
  onFocus={() => console.log('On focus FIRED 🚀')}
  onBlur={() => console.log('On blur FIRED 🚀')}
/>
```

usage with icon emojis + descriptions.

```jsx
<Dropdown 
  options={[
    {key: 'one', value: 'I have a description', icon: '🍓', description: 'I am an option with a cool fancy description on the bottom!'},
    {key: 'two', value: 'Option 2', icon: '🍓', description: 'If you pick me, yay!'},
    {key: 'three', value: 'Watermelon', icon: '🍓', description: 'Should we make watermelons smoothies?'},
  ]}
  placeholder="Select an option (w/ a description!)"
  size="large"
  onChange={() => console.log('On change FIRED 🚀')}
  onFocus={() => console.log('On focus FIRED 🚀')}
  onBlur={() => console.log('On blur FIRED 🚀')}
/>
```

## Building the project + example 

Clone the repository
```bash
git clone <path_to_repo>
```

Install the dependancies 
```bash
cd  <path_to_repo>
yarn install
```

Start up the component library
```bash
yarn start
```

Open a new terminal tab and navigate to example directory
```bash
cd <path_to_repo>/example 
```

Install the dependancies for the example react app
```bash
yarn install
```

Start the development server. A browser window should open with some examples of the dropdown :)
```bash
yarn start
```

## Props

| Prop | usage  |
| :---:   | :-: |
| Options | options take a key (string), value (string), icon (emoji/string), and a description (string).   |
| placeholder | the placeholder before an option is selected (string)|
| size | can be 'small', 'medium' or 'large' (string)|
| onChange | an function thats fired when an option is selected|
| onFocus | an function thats fired on component focus |
| onBlur | an function thats fired on component blur |

## License

MIT © [ejdweck](https://github.com/ejdweck)
