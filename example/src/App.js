import React, { Component } from 'react'

import Dropdown from 'dropdown'
import styled from 'styled-components'

const StyledContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center; 
  justify-content: center;
  background: linear-gradient(#e66465, #9198e5);
  width: 100%;
  height: 100vh;
`

const StyledDropdownContainer = styled.div`
  margin-top: 20px;
`

const StyledDropdownCollection = styled.div`
  padding: 20px;
  background-color: white;
`

export default class App extends Component {
  render () {
    return (
      <StyledContainer>
        <StyledDropdownCollection>
          <StyledDropdownContainer>
            <Dropdown 
              options={[
                { key: 'one', value: 'A simple option' },
                { key: 'two', value: 'A second option' },
                { key: 'three', value: 'Third and final!' },
                { key: 'four', value: 'mangos' },
              ]}
              placeholder="Select an Option"
              size="small"
              onChange={() => console.log('On change FIRED 🚀')}
              onFocus={() => console.log('On focus FIRED 🚀')}
              onBlur={() => console.log('On blur FIRED 🚀')}
            />
          </StyledDropdownContainer>
          <StyledDropdownContainer>
            <Dropdown 
              options={[
                {key: 'one', value: 'strawberry', icon: '🍓'},
                {key: 'two', value: 'watermelon', icon: '🍉' },
                {key: 'three', value: 'mango', icon: '🥭' },
                {key: 'four', value: 'avocado', icon: '🥑' },
                {key: 'five', value: 'pear', icon: '🍐' },
              ]}
              placeholder="Select an option (w/ a cute fruit icon!)"
              size="medium"
              onChange={() => console.log('On change FIRED 🚀')}
              onFocus={() => console.log('On focus FIRED 🚀')}
              onBlur={() => console.log('On blur FIRED 🚀')}
            />
          </StyledDropdownContainer>
          <StyledDropdownContainer>
            <Dropdown 
              options={[
                {key: 'one', value: 'I have a description', icon: '🍓', description: 'I am an option with a cool fancy description on the bottom!'},
                {key: 'two', value: 'Option 2', icon: '🍓', description: 'If you pick me, yay!'},
                {key: 'three', value: 'Watermelon', icon: '🍓', description: 'Should we make watermelons smoothies?'},
              ]}
              placeholder="Select an option (w/ a description!)"
              size="large"
              onChange={() => console.log('On change FIRED 🚀')}
              onFocus={() => console.log('On focus FIRED 🚀')}
              onBlur={() => console.log('On blur FIRED 🚀')}
            />
          </StyledDropdownContainer>
        </StyledDropdownCollection>
      </StyledContainer>
    )
  }
}
